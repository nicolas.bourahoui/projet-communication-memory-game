const cards = document.querySelectorAll('.memory-card');

let hasFlippedCard = false;
let lockBoard = false;
let firstCard, secCard;
let numberOfmoves = 0;
let correctMoves = 0;
let movesWin = 0;

function flipCard(){
    if (lockBoard) return;
    if (this === firstCard) return; 
    this.classList.toggle('flip');
    
    if (!hasFlippedCard){
        //premier click
        hasFlippedCard = true;
        firstCard = this;

        return;
    }

    //second click
    secCard = this;
    
    checkMatch();
}

function checkMatch(){
    numberOfmoves++;
    let isMatch = firstCard.dataset.framework === secCard.dataset.framework;
    isMatch ? disableCards() : unflipCards();
}

function disableCards() {
    correctMoves++
    //choisir en fonction du level joué
    if(correctMoves==movesWin) {
	    alert("Vous avez gagné!!\n" + " Vous avez gagné en " + numberOfmoves + " mouvements. \n" + "Vous pouvez cliquer sur le bouton triche!");
    }
    firstCard.removeEventListener('click', flipCard);
    secCard.removeEventListener('click', flipCard);

    resetBoard();
}

function unflipCards(){
    lockBoard = true;

    setTimeout (() => {
        firstCard.classList.remove('flip')
        secCard.classList.remove('flip')

        resetBoard();
        }, 1500);
}

function resetBoard() {
    [hasFlippedCard, lockBoard] = [false, false];
    [firstCard, secCard] = [null, null];
}


(function shuffle() {
    movesWin = cards.length/2;
    cards.forEach(card => {
        //choisir le multiple en fonction du nombre de carte (level)
        let randomPos = Math.floor(Math.random() * cards.length);
        card.style.order = randomPos
    });
})();

cards.forEach(card => card.addEventListener('click', flipCard));